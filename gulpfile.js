var fs = require('fs');
var pathJS = require('path');


// var log = require('gulplog');
var gulp = require('gulp');
var watch = require('gulp-watch');
var pug = require('gulp-pug');
var puglint = require('gulp-pug-lint2');
var data = require('gulp-data');
var merge = require('gulp-merge-json');
var tap = require('gulp-tap');
var browserSync = require('browser-sync');
var sass = require('gulp-sass');
var postcss = require('gulp-postcss');
var autoprefixer = require('autoprefixer');
var concat = require('gulp-concat');
var uglify = require('gulp-uglify');
var browserify = require('browserify');
var eslint = require('gulp-eslint');
var source = require('vinyl-source-stream');
var buffer = require('vinyl-buffer');
var rename = require('gulp-rename');
var dirSync = require('gulp-directory-sync');
var csso = require('gulp-csso');
var sourcemaps = require('gulp-sourcemaps');

var buildTimestamp = new Date().getTime();

var path = {
    dev: __dirname + "/build",
    prod: __dirname + "/prod",
    src: __dirname + "/src",
    node_modules: __dirname + "/node_modules",
    components: __dirname + "/src/components",
    vendor: __dirname + "/src/vendor",
    assets: __dirname + "/src/assets",
    fonts: __dirname + "/src/assets/fonts",
    img: __dirname + "/src/assets/img",
    pages: __dirname + "/src/pages",
    jsEntry: __dirname + "/src/entrypoint.js",
    sassEntry: __dirname + "/src/entrypoint.sass",
    styleFileName: "style.css",
    scriptFileName: "bundle.js",
};


gulp.task('js:dev', function () {
  var b = browserify({
    paths: ['./node_modules', path.src, path.vendor + "/js/", path.components],
    entries: path.jsEntry,
    debug: true,
  })
  .transform(
      "babelify",
      {
          global: true,
          ignore: [/\/node_modules\//, /\/src\/vendor\//],
          presets: ["es2015"]
      }
  );

  return b.bundle()
    .pipe(source('entrypoint.js'))
    .pipe(buffer())
        // .pipe(uglify())
        // .on('error', log.error)
    .pipe(rename(path.scriptFileName))
    .pipe(gulp.dest(path.dev));
});
gulp.task('js:prod', function () {
  var b = browserify({
    paths: ['./node_modules', path.src, path.vendor + "/js/", path.components],
    entries: path.jsEntry,
    debug: false,
  })
  .transform(
      "babelify",
      {
          global: true,
          ignore: [/\/node_modules\//, /\/src\/vendor\//],
          presets: ["es2015"]
      }
  );

  return b.bundle()
    .pipe(source('entrypoint.js'))
    .pipe(buffer())
        .pipe(uglify())
        // .on('error', log.error)
    .pipe(rename(path.scriptFileName))
    .pipe(gulp.dest(path.prod));
});
gulp.task('js:lint', function () {
  return gulp.src([path.components + "**/*.js", path.jsEntry])
      .pipe(eslint({
          configFile: "./.eslintrc"
      }))
      .pipe(eslint.format())
      .pipe(eslint.failAfterError());
});


gulp.task('css:dev', function () {
  return gulp.src(path.sassEntry)
    .pipe(sourcemaps.init())
    .pipe(sass({
        indentedSyntax: true,
        indentType: 'tab',
        indentWidth: 1,
        outputStyle: "expanded",
        data: {
            pathFonts: path.fonts,
            pathImg: path.img,
            pathVendor: path.vendor,
            pathAssets: path.assets,
            pathComponents: path.components,
            pathNodeModules: path.node_modules,
            debug: true,
            closeAreaColor: "red",
        }
    }))
    .pipe(postcss([
        autoprefixer({
            browsers: ['last 2 versions'],
            cascade: false
        })
    ]))
    .pipe(csso({
        restructure: false
    }))
    .pipe(sourcemaps.write())
    .pipe(rename(path.styleFileName))
    .pipe(gulp.dest(path.dev));
});
gulp.task('css:prod', function () {
  return gulp.src(path.sassEntry)
    // .pipe(sourcemaps.init())
    .pipe(sass({
        indentedSyntax: true,
        indentType: 'tab',
        indentWidth: 1,
        outputStyle: "expanded",
        data: {
            pathFonts: path.fonts,
            pathImg: path.img,
            pathVendor: path.vendor,
            pathAssets: path.assets,
            pathComponents: path.components,
            pathNodeModules: path.node_modules,
            debug: true,
            closeAreaColor: "red",
        }
    }))
    .pipe(postcss([
        autoprefixer({
            browsers: ['last 2 versions'],
            cascade: false
        })
    ]))
    .pipe(csso({
        restructure: false
    }))
    // .pipe(sourcemaps.write())
    .pipe(rename(path.styleFileName))
    .pipe(gulp.dest(path.prod));
});


gulp.task('html:data', function() {
    return gulp.src(path.components + '/**/*.json')
        .pipe(merge({
            fileName: 'data.json',
            edit: (json, file) => {
                // Extract the filename and strip the extension
                var filename = pathJS.basename(file.path),
                    primaryKey = filename.replace(pathJS.extname(filename), '');

                // Set the filename as the primary key for our JSON data
                var data = {};
                data[primaryKey] = json;

                return data;
            }
        }))
        .pipe(gulp.dest('/tmp'));
});
gulp.task('html:dev', ['html:data'], function () {
  gulp.src(path.pages + '/*.pug')
    .pipe(data(function() {
        return JSON.parse(fs.readFileSync('/tmp/data.json'))
    }))
    .pipe(pug({
        basedir: path.components,
        pretty: false,
        data: {
            path: path,
            debug: true,
            buildTimestamp: buildTimestamp
        }
    }))
    .pipe(gulp.dest(path.dev))
});
gulp.task('html:prod', ['html:data'], function () {
  gulp.src(path.pages + '/*.pug')
    .pipe(data(function() {
        return JSON.parse(fs.readFileSync('/tmp/data.json'))
    }))
    .pipe(pug({
        basedir: path.components,
        pretty: false,
        data: {
            path: path,
            debug: false,
            buildTimestamp: buildTimestamp
        }
    }))
    .pipe(gulp.dest(path.prod))
});
gulp.task('html:lint', function () {
    return gulp.src(path.src + '/**/*.pug')
        .pipe(puglint({
            failOnError: true
        }));
});


// Sync dir src/assets with build/assets
gulp.task('assets:dev', function () {
  return gulp.src('')
    .pipe(dirSync(path.assets, path.dev + '/assets'));
});
gulp.task('assets:prod', function () {
  return gulp.src('')
    .pipe(dirSync(path.assets, path.prod + '/assets'));
});


// browser-sync server setup
gulp.task('browserSync', function () {
  browserSync.init({
    server: {
      baseDir: path.dev
    },
    ui: {
      port: 8801
    },
    port: 8800,
    open: false,
    notify: false,
    ghostMode: false
  });
});
// browser-sync reload task
gulp.task('reload', function () {
  gulp.src(path.dev + '/**/*')
    .pipe(browserSync.reload({
      stream: true
    }));
});

gulp.task("html:reload", ['html:dev'], function() {
  gulp.src(path.dev + '/**/*')
    .pipe(browserSync.reload({
      stream: true
    }));
});
gulp.task("assets:reload", ['assets:dev'], function() {
  gulp.src(path.dev + '/**/*')
    .pipe(browserSync.reload({
      stream: true
    }));
});
gulp.task("css:reload", ['css:dev'], function() {
  gulp.src(path.dev + '/**/*')
    .pipe(browserSync.reload({
      stream: true
    }));
});
gulp.task("js:reload", ['js:dev'], function() {
  gulp.src(path.dev + '/**/*')
    .pipe(browserSync.reload({
      stream: true
    }));
});


// Main watch task
gulp.task('watch:dev', function () {
  gulp.watch(
    path.src + '/**/*.js',
    ['js:reload']);
  gulp.watch(
    [path.src + '/**/*.sass', path.src + '/**/*.css'],
    ['css:reload']);
  gulp.watch(
    [path.src + '/**/*.pug', path.src + '/**/*.md', path.components + '/**/*.json'],
    ['html:reload']);
  gulp.watch(
    path.assets + '/**',
    ['assets:reload']);
});
gulp.task('watch:prod', function () {
  gulp.watch(
    path.src + '/**/*.js',
    ['js:prod']);
  gulp.watch(
    path.src + '/**/*.sass',
    ['css:prod']);
  gulp.watch(
    [path.src + '/**/*.pug', path.components + '/**/*.json'],
    ['html:prod']);
  gulp.watch(
    path.assets + '/**',
    ['assets:prod']);
  gulp.watch(
    path.prod + '/**/*',
    ['reload']);
});


gulp.task('lint', ['js:lint', 'css:lint', 'html:lint']);
gulp.task('build:dev', ['js:dev', 'css:dev', 'html:dev', 'assets:dev']);
gulp.task('build:prod', ['js:prod', 'css:prod', 'html:prod', 'assets:prod']);

gulp.task('default', ['build:dev', 'browserSync', 'reload', 'watch:dev']);
