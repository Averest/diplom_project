class Input {
    static init() {
        $('.Input').each((i, item) => {
            new Input($(item));
        })
    }

    constructor($instance) {
        this.$instance = $instance;
        this.$instance.data('Input', this);

        this.$input = $instance.find('input');
        this.isValid = false;

        this.handle();
    }

    handle() {
        this.$input.on('input', (e) => {
            this.handleInput(e);
        })

    }

    handleInput(e) {
        this.isValid = this.validate();

        if(this.$input.closest('.Form').length) {
            this.$instance.closest('.Form').data('Form').handleInputs();
        }
    }

    validate() {
        if(this.$input.val()) {
            return true;
        }
        return false;
    }
}
export default Input;
