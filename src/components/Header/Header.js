class Header {
    static init() {
        $('.Header').each((i, item) => {
            new Header($(item));
        });
    }

    constructor($instance) {
        this.$instance = $instance;
        this.$instance.data('Header', this);

        this.$headerMenu = this.$instance.find('.Header-menu');
        this.$headerMenuButton = this.$instance.find('.Header-buttonShow');
        this.$headerMenuCloseButton = this.$instance.find('.Header-menu .Header-buttonClose');
        this.$hider = this.$instance.find('.Header-hider');

        this.handle();
    }

    handle() {
        this.$headerMenuButton.on('click', (e) => {
            e.preventDefault();
            this.openHeaderMenu();
        });
        this.$headerMenu.on('click', (e) => {
            if(!$(e.currentTarget).hasClass('isHidden')) {
                e.stopPropagation();
            }
        });
        this.$headerMenuCloseButton.on('click', (e) => {
            e.preventDefault();
            this.closeHeaderMenu();
        });

        $(window).on('resize', (e) => {
            this.closeHeaderMenu();
        })
    }

    openHeaderMenu() {
        this.$headerMenu.removeClass("isHidden");
        $('body').addClass('isRestrictScroll');
    }

    closeHeaderMenu() {
        this.$headerMenu.addClass("isHidden");
        $('body').removeClass('isRestrictScroll');
    }
}

export default Header;
