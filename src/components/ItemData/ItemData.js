class ItemData {
    static init() {
        $('.ItemData').each((i, item) => {
            new ItemData($(item));
        });
    }

    constructor($instance) {
        this.$instance = $instance;
        this.$instance.data('ItemData', this);

        this.$openButton = this.$instance.find('.ItemData-button');
        this.$chekout = this.$instance.find('#Checkout');
        this.$openPolicy = this.$instance.find('.Policy-button');
        this.$policy = this.$instance.find('#Policy');

        this.handle();
    }

    handle() {
        this.$openButton.on('click', (e) => {
            e.preventDefault();
            this.$chekout.removeClass('isHidden');
            $('body').addClass('isRestrictScroll');
            });
        this.$openPolicy.on('click', (e) => {
            e.preventDefault();
            this.$policy.removeClass('isHidden');
            $('body').addClass('isRestrictScroll');
        });
        }
    }

export default ItemData;
