class Form {
    static init() {
        $('.Form').each((i, item) => {
            new Form($(item));
        })
    }

    constructor($instance) {
        this.$instance = $instance;
        this.$instance.data('Form', this);
        this.$button = $instance.find('.Button');
        this.$input = $instance.find('input')

        this.handle();
    }

    handle() {
        this.$instance.on('submit', (e) => {
            this.handleSubmit(e);
        });
        this.handleInputs();
    }

    handleSubmit(e) {
        e.preventDefault();
        alert("Форма заполнена");
    }

    handleInputs() {
        let check = true;
        this.$input.each((i,item) => {
            if(check) {
                check = $(item).closest('.Input').data('Input').isValid;
            }
        });
        if(
            check
        ) {
            this.$button.removeClass('isDisabled');
        }
    }

}
export default Form;
