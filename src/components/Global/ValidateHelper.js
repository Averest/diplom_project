class ValidateHelper {
    static emailValidate(email) {
        return email.match(/[a-z0-9!#$%&'*+\/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+\/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?/) ? true : false;
    }
    static phoneValidate(phone) {
        return phone.match(/\+?\d{10,15}/) ? true : false;
    }
}

export default ValidateHelper;
