class Modal {
    static init() {
        $('.Modal').each((i, item) => {
            new Modal($(item));
        });
    }

    constructor($instance) {
        this.$instance = $instance;
        this.$instance.data('Modal', this);

        this.$ModalClose = this.$instance.find('.Modal-close');

        this.handle();
    }

    handle() {
        this.$ModalClose.on('click', (e) => {
            e.preventDefault();
            this.$instance.addClass('isHidden');
            $('body').removeClass('isRestrictScroll');
        });
    }

}
export default Modal;
