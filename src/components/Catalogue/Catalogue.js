class Catalogue {
    static init() {
        $('.Catalogue').each((i, item) => {
            new Catalogue($(item));
        });
    }

    constructor($instance) {
        this.$instance = $instance;
        this.$instance.data('Catalogue', this);

        this.$capacity_block = this.$instance.find('.Catalogue-right');
        this.$capacity_list = this.$instance.find('.Catalogue-modal');
        this.$button_select = this.$instance.find('.Catalogue-block');
        this.$text_button = this.$instance.find('.Label');
        this.$capacity = this.$instance.find('capacity');

        this.$count = 0;
        this.handle();
    }

    handle() {
        this.$capacity_block.on('click', (e) => {
            e.preventDefault();
            e.stopPropagation();
            this.$capacity_list.toggleClass('isHidden');
        })

        this.$text_button.on('click', (e) => {
            e.preventDefault();
            this.handleCapacityChange(e);
        })

        $(window).on('click', (e) => {
            this.closeList();
        })
    }

    handleCapacityChange(e) {
        this.activeCapacity(
        $(e.currentTarget).attr('data-capacity'));
    }

    activeCapacity(capacity) {
        this.$instance.find('.Catalogue-window').removeClass('isVisible');
        this.$instance.find('.Catalogue-window[data-i=' + capacity + ']').addClass('isVisible');
        this.$instance.find('.Capacity').text(capacity + ' кг');
    }

    closeList() {
        this.$capacity_list.addClass('isHidden');
    }

}

export default Catalogue;
