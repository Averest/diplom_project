// include jQuery with standart object
global.$ = require('jquery.min.js');
global.jQuery = $;
require('jquery-ui.js');
require('jquery-ui.combobox');
import Global from 'Global/Global.js';
import Header from 'Header/Header.js';
import Catalogue from 'Catalogue/Catalogue.js';
import Modal from 'Modal/Modal.js';
import ItemData from 'ItemData/ItemData.js'
import Input from 'Input/Input.js'
import Form from 'Form/Form.js'


$(document).on('mobileinit', function() {
    $.mobile.loadingMessage = false;
    $.mobile.loader.prototype.options.disabled = true;
    $.mobile.loading( "hide" );
    $.mobile.loading().hide();
    $.mobile.ajaxEnabled=false;
    $.mobile.autoInitializePage=false;
    $.mobile.linkBindingEnabled=false;
    $.mobile.hashListeningEnabled=false;
});

$(document).ready(function() {
    $(".ui-loader").remove();

    Global.init();
    Header.init();
    Catalogue.init();
    Modal.init();
    ItemData.init();
    Input.init();
    Form.init();
    // const one = basicScroll.create({
    //     elem: document.querySelector('.one'),
    //     direct: true,
    //     from: 'bottom-bottom', to: 'bottom-bottom',
    //     props: {
    //         '--opacity': {from: .01, to: .99},
    //         '--trans': {from: 10, to: 0}
    //     }
    // })
    // one.start()
});
